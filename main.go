// Программа принимает строку типа "3 / 2" или "V + X"
// производит вычисления и выводит результат в римском или арабском представлении чисел.
//
// Автор: Alexandr
// Дата: 12.07.2023

package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	var (
		err       error
		operand   rune
		number1   int
		number2   int
		isArabian bool
	)

	// считывание строки из stdin
	reader := bufio.NewReader(os.Stdin)
	input, _ := reader.ReadString('\n')
	input = strings.TrimSpace(input)
	values := strings.Split(input, " ")

	// проверка на присутствие только 3-х слов в строке
	if len(values) == 3 {
		// перевод второго слова в символ (ожидается арифметический оператор)
		if len(values[1]) == 1 {
			operand = rune(values[1][0])
		} else {
			err = errors.New("ошибка при считывании оператора")
		}
	} else {
		err = errors.New("ошибка при считывании выражения")
	}

	if err == nil {
		number1, number2, isArabian, err = convert_input_values(values[0], values[2])
	}

	if (!(number1 > 0 && number1 < 11) || !(number2 > 0 && number2 < 11)) && (err == nil) {
		if isArabian {
			err = errors.New("программа способна считывать числа от 1 до 10 включительно")
		} else {
			err = errors.New("программа способна считывать числа от I до X включительно")
		}
	}

	var result int
	if err == nil {
		result, err = calculate(number1, number2, operand)

		if !isArabian && result < 1 && err == nil {
			err = errors.New("у древних римлян небыло чисел меньше 1")
		}
	}

	if err == nil {
		err = printResult(result, isArabian)
	}

	if err != nil {
		fmt.Println(err)
	}
}

// Переводит в int строки с числом в арабском или римском представлении
// Проверяет на соответствие системам счисления
func convert_input_values(value1, value2 string) (number1, number2 int, isArabian bool, err error) {
	var isArabian1 bool

	number1, isArabian1, err = convert_value(value1)
	if err != nil {
		return number1, number2, false, err
	}

	var isArabian2 bool
	number2, isArabian2, err = convert_value(value2)
	if err != nil {
		return number1, number2, false, err
	}

	if isArabian1 == isArabian2 {
		isArabian = isArabian1
	} else {
		err = errors.New("невозможно провести операцию над числами в разных представлениях")
	}

	return number1, number2, isArabian, err
}

// переводит в int слово с числом в арабском или римском представлении
func convert_value(value string) (number int, isArabian bool, err error) {
	number, err = strconv.Atoi(value)
	if err == nil {
		isArabian = true
	} else {
		isArabian = false
		number, err = roman_to_arabic_10(value)
	}

	return number, isArabian, err
}

// Переводит римское представление чисел в int
func roman_to_arabic_10(value string) (number int, err error) {
	arab := map[string]int{
		"I":    1,
		"II":   2,
		"III":  3,
		"IV":   4,
		"V":    5,
		"VI":   6,
		"VII":  7,
		"VIII": 8,
		"IX":   9,
		"X":    10,
	}
	number, exists := arab[value]
	if !exists {
		err = errors.New("программа способна считывать числа от I до X включительно")
	}

	return number, err
}

// Производит элементарные математические вычисления
func calculate(number1, number2 int, operand rune) (result int, err error) {
	switch operand {
	case '+':
		result = number1 + number2
	case '-':
		result = number1 - number2
	case '*':
		result = number1 * number2
	case '/':
		if number2 == 0 {
			err = errors.New("деление на 0")
		}
		result = number1 / number2
	default:
		err = errors.New("неизветсный операнд")
	}

	return result, err
}

// Выводит результат в арабском или римском представлении цифр
func printResult(result int, isArabian bool) (err error) {
	if isArabian {
		fmt.Print(result)
	} else {
		roman := map[int]string{
			1:   "I",
			2:   "II",
			3:   "III",
			4:   "IV",
			5:   "V",
			6:   "VI",
			7:   "VII",
			8:   "VIII",
			9:   "IX",
			10:  "X",
			50:  "L",
			100: "C",
		}

		for result > 0 {
			if result == 100 {
				fmt.Print(roman[100])
				result -= 100
			} else if result >= 90 {
				fmt.Print(roman[10], roman[100])
				result -= 90
			} else if result >= 50 {
				fmt.Print(roman[50])
				result -= 50
			} else if result >= 40 {
				fmt.Print(roman[10], roman[50])
				result -= 40
			} else if result >= 10 {
				fmt.Print(roman[10])
				result -= 10
			} else if result < 10 {
				fmt.Print(roman[result])
				result = 0
			} else {
				err = errors.New("O_O я не умею выводить такие числа…")
			}
		}
	}

	return err
}
